#!/usr/bin/python3
# SPDX-FileCopyrightText: 2022 Volker Krause <vkrause@kde.org>
# SPDX-License-Identifier: LGPL-2.0-or-later

import argparse
import json
import os
import re
import requests

parser = argparse.ArgumentParser(description='List all enabled CI jobs for each repo in a group')
parser.add_argument('--groups', type=str, nargs='*', help='Name of the repository group to check (default: frameworks)', default=['frameworks'])
parser.add_argument('--no-static', action='store_true', help='Filter out static build job')
arguments = parser.parse_args()

gqlQuery = open(os.path.join(os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__))), 'group-ci-job-list.graphql')).read()
gqlRequest = {}
gqlRequest['query'] = gqlQuery

for group in arguments.groups:
    gqlRequest['variables'] = { 'groupName': group, 'ref': 'master' }

    req = requests.post('https://invent.kde.org/api/graphql', json = gqlRequest)
    result = json.loads(req.content)

    for project in result['data']['group']['projects']['nodes']:
        if project['archived'] == True:
            continue
        jobs = []
        for pipeline in project['pipelines']['nodes']:
            for job in pipeline['jobs']['nodes']:
                jobs.append(job['name'])
        if len(jobs) == 0:
            continue
        qt5Jobs = []
        for job in jobs:
            if 'qt5' in job or ('craft' in job and not '6' in job and not 'snapcraft' in job):
                qt5Jobs.append(job)
        qt5Jobs.sort()
        if len(qt5Jobs):
            print(f"{project['name']} ({project['fullPath']}): {', '.join(qt5Jobs)}")

#!/usr/bin/python3
# SPDX-FileCopyrightText: 2022 Volker Krause <vkrause@kde.org>
# SPDX-License-Identifier: LGPL-2.0-or-later

import argparse
import json
import os
import re
import requests

parser = argparse.ArgumentParser(description='List all enabled CI jobs for each repo in a group')
parser.add_argument('--group', type=str, help='Name of the repository group to check (default: frameworks)', default='frameworks')
parser.add_argument('--no-static', action='store_true', help='Filter out static build job')
arguments = parser.parse_args()

gqlQuery = open(os.path.join(os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__))), 'group-ci-job-list.graphql')).read()
gqlRequest = {}
gqlRequest['query'] = gqlQuery
gqlRequest['variables'] = { 'groupName': arguments.group, 'ref': 'master' }

req = requests.post('https://invent.kde.org/api/graphql', json = gqlRequest)
result = json.loads(req.content)

for project in result['data']['group']['projects']['nodes']:
    if project['archived'] == True:
        continue
    jobs = []
    for pipeline in project['pipelines']['nodes']:
        for job in pipeline['jobs']['nodes']:
            jobs.append(job['name'])
    if len(jobs) == 0:
        continue
    missingQt6Jobs = []
    for job in jobs:
        jobId = re.match('(.*)_qt(\d)\d*(_.*)?', job)
        if not jobId or jobId[2] == 6:
            continue
        qt6JobFound = False
        for job2 in jobs:
            if job2.startswith(jobId[1]) and 'qt6' in job2 and (not jobId[3] or job2.endswith(jobId[3])):
                qt6JobFound = True
                break
        if not qt6JobFound:
            missingQt6Jobs.append(job)
    if arguments.no_static:
        missingQt6Jobs = [ s for s in missingQt6Jobs if not s.endswith('_static') ]
    missingQt6Jobs.sort()
    if len(missingQt6Jobs):
        print(f"{project['name']} ({project['fullPath']}): {', '.join(missingQt6Jobs)}")

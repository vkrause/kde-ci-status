#!/usr/bin/python3
# SPDX-FileCopyrightText: 2022 Volker Krause <vkrause@kde.org>
# SPDX-License-Identifier: LGPL-2.0-or-later

import argparse
import json
import os
import requests

parser = argparse.ArgumentParser(description='Check the CI status of an entire repository group')
parser.add_argument('--group', type=str, help='Name of the repository group to check (default: frameworks)', default='frameworks')
parser.add_argument('--branch', type=str, help='Name of the branch to check (default: master)', default='master')
parser.add_argument('--only-failures', help='Omit repositories without issues', action='store_true')
arguments = parser.parse_args()

gqlQuery = open(os.path.join(os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__))), 'group-ci-status.graphql')).read()
gqlRequest = {}
gqlRequest['query'] = gqlQuery
gqlRequest['variables'] = { 'groupName': arguments.group, 'ref': arguments.branch }

req = requests.post('https://invent.kde.org/api/graphql', json = gqlRequest)
result = json.loads(req.content)

projectCount = 0
buildFailCount = 0
testFailCount = 0

for project in result['data']['group']['projects']['nodes']:
    if project['archived'] == True:
        continue
    # determine the last CI run that produced any result
    # we have to do this to avoid getting wrong results due to skipped/cancelled/running pipelines
    pipeline = {}
    if project['lastSuccess']['nodes'] and len(project['lastSuccess']['nodes']) > 0:
        pipeline = project['lastSuccess']['nodes'][0]
    lastFailure = project['lastFailure']['nodes']
    if lastFailure and len(lastFailure) > 0 and (not pipeline or lastFailure[0]['finishedAt'] > pipeline['finishedAt']):
        pipeline = lastFailure[0]

    if not pipeline:
        print(f"{project['name']} has no CI!")
        continue

    projectCount += 1
    if pipeline['status'] != 'SUCCESS':
        buildFailCount += 1
    if pipeline['testReportSummary']['total']['failed'] > 0:
        testFailCount += 1
    if arguments.only_failures and pipeline['status'] == 'SUCCESS' and pipeline['testReportSummary']['total']['failed'] == 0:
        continue

    print(project['name'])
    if pipeline['status'] == 'SUCCESS':
        print(' ✅ Build')
    else:
        print(f" 💥 Build: https://invent.kde.org/{pipeline['detailedStatus']['detailsPath']}")
        for job in pipeline['jobs']['nodes']:
            if job['status'] == 'SUCCESS':
                continue
            print(f"   {job['name']} - https://invent.kde.org/{job['detailedStatus']['detailsPath']}")
    if pipeline['testReportSummary']['total']['failed'] == 0:
        print(' ✅ Tests')
    else:
        print(f" ⚠️ Tests: https://invent.kde.org/{pipeline['detailedStatus']['detailsPath']}")
        for suite in pipeline['testReportSummary']['testSuites']['nodes']:
            if suite['failedCount'] > 0:
                print(f"   {suite['name']} ({suite['failedCount']}/{suite['totalCount']})")

print(f"\nBuild issues: {buildFailCount}/{projectCount}\nTest issues: {testFailCount}/{projectCount}")

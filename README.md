# KDE CI Status Query Scripts

## Features

* Query last success/failure status of CI pipeline in all repositories of a given group.
* Query last test report for all repositories in a given group.
* List all jobs that were run in the last completed pipeline run for all respositories in a group.

## Usage Warning

Use only when absolutely needed and in coordination with KDE sysadmins, this can cause unreasonable load on the infrastructure!

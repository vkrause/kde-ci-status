#!/usr/bin/python3
# SPDX-FileCopyrightText: 2022 Volker Krause <vkrause@kde.org>
# SPDX-License-Identifier: LGPL-2.0-or-later

import argparse
import json
import os
import requests

parser = argparse.ArgumentParser(description='List all enabled CI jobs for each repo in a group')
parser.add_argument('--group', type=str, help='Name of the repository group to check (default: frameworks)', default='frameworks')
arguments = parser.parse_args()

gqlQuery = open(os.path.join(os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__))), 'group-ci-job-list.graphql')).read()
gqlRequest = {}
gqlRequest['query'] = gqlQuery
gqlRequest['variables'] = { 'groupName': arguments.group, 'ref': 'master' }

req = requests.post('https://invent.kde.org/api/graphql', json = gqlRequest)
result = json.loads(req.content)

for project in result['data']['group']['projects']['nodes']:
    if project['archived'] == True:
        continue
    jobs = []
    for pipeline in project['pipelines']['nodes']:
        for job in pipeline['jobs']['nodes']:
            jobs.append(job['name'])
    jobs.sort()
    if len(jobs) > 0:
        j = ", ".join(jobs)
        print(f"{project['name']} ({project['fullPath']}): {j}")
    else:
        print(f"{project['name']} ({project['fullPath']}): <none>")
